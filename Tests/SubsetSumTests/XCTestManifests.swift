import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
  [
    testCase(SubsetSumTests.allTests),
  ]
}
#endif
