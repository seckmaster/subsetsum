import XCTest
@testable import SubsetSum

final class SubsetSumTests: XCTestCase {
  func testDP() {
    XCTAssertEqual(8, dynamicProgrammingSS(A: [1, 4, 5, 7], k: 8))
    XCTAssertEqual(9, dynamicProgrammingSS(A: [1, 4, 5, 7], k: 9))
    XCTAssertEqual(12, dynamicProgrammingSS(A: [1, 4, 5, 7], k: 12))
    XCTAssertEqual(17, dynamicProgrammingSS(A: [1, 4, 5, 7], k: 17))
  }
  
  func testBreadth() {
    XCTAssertEqual(8, breadthSearchSS(A: [1, 4, 5, 7], k: 8))
    XCTAssertEqual(9, breadthSearchSS(A: [1, 4, 5, 7], k: 9))
    XCTAssertEqual(12, breadthSearchSS(A: [1, 4, 5, 7], k: 12))
    XCTAssertEqual(17, breadthSearchSS(A: [1, 4, 5, 7], k: 17))
  }
  
  func testGreedy() {
    XCTAssertEqual(8, greedySS(A: [1, 4, 5, 7], k: 8))
    XCTAssertEqual(12, greedySS(A: [1, 4, 5, 7], k: 12))
    XCTAssertEqual(6, greedySS(A: [5, 6, 5], k: 10))
  }
  
  func testFPTASS() {
    XCTAssertEqual(302, FPTASSS(A: [104, 102, 201, 101], k: 308, ε: 0.4))
    XCTAssertEqual(308, FPTASSS(A: [104, 102, 201, 101], k: 308, ε: 0))
  }
}
