import Foundation
import SubsetSum

typealias SubsetSumProblem = (A: [Int], k: Int)
typealias SubsetSumSolver = ([Int], Int) -> Int

func parser(_ text: String) -> SubsetSumProblem {
  let lines = text.split(separator: "\n")
  let array = lines
    .dropFirst(2)
    .map { Int($0)! }
  let k = Int(lines[1])!
  return (array, k)
}

func openAndParseFile(_ path: String) throws -> SubsetSumProblem {
  let content = String(data: try Data(contentsOf: URL(fileURLWithPath: path)), encoding: .ascii)!
  return parser(content)
}

func convert(_ f: @escaping ([Int], Int) -> Int) -> (SubsetSumProblem) -> Int {
  { f($0.A, $0.k) }
}

func FPTASSS(ε: Float) -> SubsetSumSolver {
  { FPTASSS(A: $0, k: $1, ε: ε) }
}

let allSolvers: [(SubsetSumSolver, String)] = [
//  (dynamicProgrammingSS, "dynamic programming"),
//  (breadthSearchSS, "breadth search"),
//  (greedySS, "greedy"),
  (FPTASSS(ε: 0.2), "FPTASSS 0.2"),
  (FPTASSS(ε: 0.4), "FPTASSS 0.4"),
  (FPTASSS(ε: 0.6), "FPTASSS 0.6"),
  (FPTASSS(ε: 0.8), "FPTASSS 0.8")
]

func executeFile(_ path: String, solvers: [(SubsetSumSolver, String)] = allSolvers) throws {
  let (A, k) = try openAndParseFile(path)
  print("✅ | Parsed \(path)")
  print("Goal:", k)
  
  for (solver, name) in allSolvers {
    let (time, val) = benchmark { solver(A, k) }
    print("\(name) solved with value: \(val) in \(String(format: "%.5f", time))s")
    let opt = dynamicProgrammingSS(A: A, k: k)
    print("  - error: \((Double(opt) - Double(val))/Double(opt))")
  }
  print()
}

if CommandLine.argc > 1 {
  /// 1. Assignment
  
  let path = CommandLine.arguments[1]
  
  var isDirectory: ObjCBool = false
  FileManager.default.fileExists(atPath: path, isDirectory: &isDirectory)
  
  switch isDirectory.boolValue {
  case true:
    _ = try FileManager.default.contentsOfDirectory(atPath: path)
      .sorted()
      .map { try executeFile(path + $0) }
    
//    _ = try FileManager.default.contentsOfDirectory(atPath: path).sorted().map {
//      print($0)
//      let (A, k) = try openAndParseFile(path + $0)
//      print("A: \(A.count), k: \(k)")
//    }
  case false:
    try executeFile(path)
  }
} else {
  
  /// 2. Assingment
  
  func hardDP(n: Int, k: Int, range: Range<Int>) -> SubsetSumProblem {
    /// Just return HUGE k!
//    let a = Int.random(in: 1...10)
//    return (A: [a << 31], k: a << 31)
    (
      A: (0..<n).map { _ in Int.random(in: range) },
      k: k
    )
  }
  
  func plotDP() {
    let ks: [Double] = [10e1, 10e2, 10e3, 10e4, 10e5, 10e6, 10e7, 10e8]
    let problems: [SubsetSumProblem] = ks.map { hardDP(n: 10, k: Int($0), range: 1..<101)  }
    let times: [Double] = problems.map { p in
      print(p.k)
      return benchmark { dynamicProgrammingSS(A: p.A, k: p.k) }.time
//      return benchmark { breadthSearchSS(A: p.A, k: p.k) }.time
    }

    plt.xscale("log")
    scatter(
      x: ks,
      y: times
    )
  }
  
//  plotDP()
//  exit(0)
  
  func hardBreadth(n: Int, range: ClosedRange<Int> = 0...5000) -> SubsetSumProblem {
    /// Return `k` as the sum of `A`. By doing that
    /// the algorithm won't be able to perform any cutting! // TODO: - Explanation
    /// NOTE: - This one is also hard for DP if range is big!
    let A = (0..<n).map { _ in Int.random(in: range) }
    return (
      A: A,
      k: A.reduce(0, +)
    )
  }
  
  func plotBreadth() {
    let ns = [10, 20, 50, 100, 1000, 2000, 10000]
    let problems: [SubsetSumProblem] = ns.map { hardBreadth(n: $0, range: 1...100) }
    let times: [Double] = problems.map { p in
      print(p.k)
//      return benchmark { dynamicProgrammingSS(A: p.A, k: p.k) }.time
      return benchmark { breadthSearchSS(A: p.A, k: p.k) }.time
    }
    
    plt.xscale("log")
    scatter(
      x: ns.map { Double($0) },
      y: times
    )
  }
  
//  plotBreadth()
//  exit(0)
  
  func hardGreedy(range: Range<Int>) -> SubsetSumProblem {
    /// Return problem which clearly contains 100% accurate solution, but greedy returns
    /// solution accurate within [50, 75]%. As `x` approaches infinity, accuracy approaches 50%.
    /// `A = [x, x + 1, x]`, `k = 2x`
    let x = Int.random(in: range)
    return (
      A: [x, x + 1, x],
      k: 2 * x
    )
  }
  
  func plotGreedy() {
    let ranges: [Int] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10e1, 10e2, 10e3, 10e4, 10e5].map { Int($0) }
    let problems: [SubsetSumProblem] = ranges.map { hardGreedy(range: $0..<($0 + 1)) }
    let accuracies: [Double] = problems.map { p in
      print(p.k)
      let res = Double(greedySS(A: p.A, k: p.k))
      return abs(res - Double(p.k)) / Double(p.k)
    }
    
    plt.xscale("log")
    scatter(
      x: ranges.map { Double($0) },
      y: accuracies
    )
  }
  
//  plotGreedy()
//  exit(0)
  
  func hardGreedy2(n: Int, range: Range<Int>) -> SubsetSumProblem {
    // TODO: - explanation
    let rnd = Int.random(in: range)
    let A = Array(repeating: rnd, count: n)
    let k = A.reduce(0, +)
    return (
      A: A + [k - rnd + 1],
      k: k
    )
  }
  
  func plotGreedy2() {
    let ranges: [Int] = [4, 10e1, 10e2, 10e3, 10e4, 10e5].map { Int($0) }
    let problems: [SubsetSumProblem] = ranges.map { hardGreedy2(n: $0, range: 1..<2) }
    let accuracies: [Double] = problems.map { p in
      print(p.k)
      let res = Double(greedySS(A: p.A, k: p.k))
      return abs(res - Double(p.k)) / Double(p.k)
    }
    
    plt.xscale("log")
    scatter(
      x: ranges.map { Double($0) },
      y: accuracies
    )
  }

//  plotGreedy2()
//  exit(0)
  
  func hardFPTAS(n: Int, range: ClosedRange<Int>) -> SubsetSumProblem {
    let k = Int.random(in: range)
    assert(k - n > 0)
    let A = [k] + (1..<n).map { k - $0 }
    return (A, k)
  }

  /// Hard DP
//  print(convert(breadthSearchSS)(hardDP()))
//  print(convert(FPTASSS(ε: 0.5))(hardDP()))
//  print(convert(dynamicProgrammingSS)(hardDP()))
  
  /// Hard breadth
//  print(convert(dynamicProgrammingSS)(hardBreadth(n: 1000)))
//  print(convert(FPTASSS(ε: 0.5))(hardBreadth(n: 1000)))
//  print(convert(breadthSearchSS)(hardBreadth(n: 1000)))
  
  /// Hard greedy
//  let hardProblemForGreedy = hardGreedy()
//  print(Double(convert(greedySS)(hardProblemForGreedy)) / Double(hardProblemForGreedy.k))
  
  ///
  
  func plotFPTAS(h: Float = 0.05, path: String = "/Users/tonikocjan/swift/ANA/SubsetSum/Examples/ss5") throws {
    let (A, k) = try openAndParseFile(path)
    let epsilons = Array(stride(from: Float(1), to: 0, by: -h))
    let times = epsilons.map { (eps: Float) -> Double in
      print(eps)
      return benchmark(trials: 10) { _ = FPTASSS(A: A, k: k, ε: eps) }
    }
    scatter(x: epsilons.map { Double($0) }, y: times, title: String(path.split(separator: "/").last!))
  }
  
  try plotFPTAS()
  
  let (A, k) = hardFPTAS(n: 10, range: 100...100)
  let opt = A.max()!
  print(A.max()!, A.min()!)
  let (time, val) = benchmark { FPTASSS(A: A, k: k, ε: 0.25) }
  print("FPTAS 0.2 solved with value: \(val) in \(String(format: "%.5f", time))s")
  print("  - error: \((Double(opt) - Double(val))/Double(opt))")
}
