//
//  Plot.swift
//  SwifyPy
//
//  Created by Toni Kocjan on 28/03/2020.
//

import PythonKit
import Foundation

public typealias MatrixScalar = FloatingPoint & BinaryFloatingPoint

public func scatter<T: MatrixScalar>(
  x: [T],
  y: [T],
  color: String? = nil,
  title: String? = nil,
  show: Bool = true) where T: PythonConvertible
{
  plt.scatter(x, y, c: color)
  plt.title(title)
  if show { plt.show() }
}

public func scatter<T: MatrixScalar>(y: [T]) where T: PythonConvertible {
  plt.scatter(Array(0..<y.count), y)
  plt.show()
}
