public func dynamicProgrammingSS(A: [Int], k: Int) -> Int {
  let width = k + 1
  let size = (A.count + 1) * width
//  var dp = [Int](repeating: 0, count: size)
//  for i in 0..<A.count {
//    for j in 1...k {
//      let a1 = dp[i * width + j]
//      let a2 = (j - A[i] >= 0) ? (dp[i * width + (j - A[i])] + A[i]) : -1
//      dp[(i + 1) * width + j] = max(a1, a2)
//    }
//  }
  
  let dp = UnsafeMutablePointer<Int>.allocate(capacity: size)
  for i in 0...A.count {
    dp.advanced(by: i * width).initialize(to: 0)
  }
  for i in 0...k {
    dp.advanced(by: i).initialize(to: 0)
  }

  for i in 0..<A.count {
    for j in 1...k {
      let a1 = dp.advanced(by: i * width + j).pointee
      let a2 = (j - A[i] >= 0) ? (dp.advanced(by: i * width + (j - A[i])).pointee + A[i]) : -1
      dp.advanced(by: (i + 1) * width + j).initialize(to: max(a1, a2))
    }
  }
  
  return dp[A.count * width + k]
}

public func breadthSearchSS(A: [Int], k: Int) -> Int {
  var L = Set<Int>()
  L.insert(0)
  var best = 0
  for el in A {
    for a in L where a + el <= k {
      if a + el > best { best = a + el }
      L.insert(a + el)
    }
  }
  return best
}

public func greedySS(A: [Int], k: Int) -> Int {
  let A = A.sorted(by: >)
  var subsetSum = 0
  for a in A where a <= k - subsetSum {
    subsetSum += a
  }
  return subsetSum
}

public func FPTASSS(A: [Int], k: Int, ε: Float) -> Int {
  func trim(L: [Int], δ: Float) -> [Int] {
    var L_ = [Int]()
    L_.reserveCapacity(L.count)
    L_.append(L[0])
    for i in 1..<L.count {
      if Float(L[i]) > Float(L_.last!) * (1 + δ) {
        L_.append(L[i])
      }
    }
//    print(L.coun““t - L_.count)
    return L_
  }
  
  let δ = ε / Float(2 * A.count)
  var L = [Int]()
  L.reserveCapacity(A.count)
  L.append(0)
  for i in 0..<A.count {
    // 1. merge&sort
    let el = A[i]
    for a in L {
      L.append(a + el)
    }
    L.sort()
    // 2. trim
    L = trim(L: L, δ: δ)
    // 3. filter
    L.removeAll { $0 > k }
  }
  return L.last!
}
