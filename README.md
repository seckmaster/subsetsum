# 2nd assignment ANA

## SubsetSum

### Generation schemes for _hard_ problems

Find random generation schemes generating hard instances where algorithms either need a lot of time to solve the problem (accurate algorithms) or performs badly - bad approximation (approximation algorithms).

#### Scheme for DYN

We prepare a scheme which takes as it's arguments:

  1. `n`: Size of the set.
  2. `k`: Target number.
  3. `range`: Range in which random numbers are generated.

We observe that we can produce hard problems by making `k` very big, even with small `n`, as shown in the following plot (`n` equals 10, `range` is `[1, 100]`):

![dp](hardDP.png)

For comparison, EXH algorithm doesn't have much problem with the same problem set:

![dp](easyBreadth.png)

#### Scheme for EXH

We prepare a scheme which takes as it's arguments:

  1. `n`: Size of the set.
  3. `range`: Range in which random numbers are generated.

One way of generating hard problems for this algorithm is to sum all values in the set and use it as the target value `k`.

This is due the fact that the algorithm won't be able to perform any cutting at each layer, because the values in the layer will always be less than `k`. 

The following graph displays how time changes with regard to `n`, where `k` is the sum of randomly generated values (`range` is `[1, 100]`):

![breadth](hardBreadth.png)

This scheme is also somewhat problematic for DYN, but still it performs better than EXH:

![dp](hardDP2.png)

#### Scheme for GREEDY

For approximation algorithms we are trying to find problems for which algorithm performs poorly in terms of accuracy. 

For greedy algorithm we prepare a simple scheme which takes as it's arguments:

  1. `range`: Range in which random numbers are generated.

The scheme generates a single random number `x` and creates a problem where set consists of `[x, x + 1, x]` and `k` equals `2x`. Such problems clearly contain an optimal - `2x`, but the algorithm finds a poor approximation of `x + 1`. The relative error for this scheme is between `0` and `0.5`, increasing as `x` gets bigger:

![dp](hardGreedy.png)

We can generalize the above scheme:

Generate `n - 1` random numbers and sum them together. Their sum is `k`. Generate another random number which must be bigger than the largest number in the generated set and smaller than their sum. The original set together with this new number represents the whole set.

#### Scheme for FPTASS

For FPTASS scheme we provide a trivial scheme in which we randomly select `k` from some range, than generate `n`, `n` << `k`, numbers `x_1 = k - 1, x_2 = k - 2, ..., k_n = k - n`. With such a scheme the FPTASS algorithm will often find the worst approximation:

```
1> let (A, k) = hardFPTAS(n: 100, range: 500...1000)
k: 1000, min(A): 901
2> FPTASSS(A: A, k: k, ε: 0.4)
FPTAS 0.4 solved with value: 901 in 0.00149s
  - error: 0.099
```

```
1> let (A, k) = hardFPTAS(n: 100, range: 500...1000)
k: 100, min(A): 91
2> FPTASSS(A: A, k: k, ε: 0.25)
FPTAS 0.25 solved with value: 91 in 0.00030s
  - error: 0.09
```

### FPTASS

The following two graphs display how the running-time of the FPTASS algorithm changes depending on the `ε`:

![fprAA](fptassBenchmark1.png)
![fprAA](fptassBenchmark2.png)