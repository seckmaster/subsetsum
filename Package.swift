// swift-tools-version:5.1
import PackageDescription

let package = Package(
  name: "SubsetSum",
  products: [
    .library(
      name: "SubsetSum",
      targets: ["SubsetSum"])
  ],
  dependencies: [
    .package(url: "https://github.com/pvieito/PythonKit.git", .branch("master")),
  ],
  targets: [
    .target(
      name: "SubsetSum",
      dependencies: ["PythonKit"]),
    .target(
      name: "Run",
      dependencies: ["SubsetSum"]),
    .testTarget(
      name: "SubsetSumTests",
      dependencies: ["SubsetSum"]),
  ]
)
